/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : localhost:3306
 Source Schema         : notepad

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 20/08/2022 22:36:09
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bill
-- ----------------------------
DROP TABLE IF EXISTS `bill`;
CREATE TABLE `bill`  (
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `utility` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `money` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `time` datetime(0) NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bill
-- ----------------------------
INSERT INTO `bill` VALUES ('111', '伙食', '买薯条', '10', '2022-06-11 00:00:00');
INSERT INTO `bill` VALUES ('111', '零食', '买鸡腿', '8', '2022-06-11 00:00:00');
INSERT INTO `bill` VALUES ('111', '娱乐', '买游戏皮肤', '88', '2022-06-11 00:00:00');
INSERT INTO `bill` VALUES ('111', '交通出行', '打车', '10', '2022-06-11 00:00:00');
INSERT INTO `bill` VALUES ('111', '生活缴费', '交水费', '100', '2022-06-11 00:00:00');
INSERT INTO `bill` VALUES ('111', '网上购物', '买棉袄', '288', '2022-06-11 00:00:00');
INSERT INTO `bill` VALUES ('111', '伙食', '吃早餐', '8', '2022-06-11 00:00:00');
INSERT INTO `bill` VALUES ('tom', '伙食', '早餐', '5', '2022-06-12 00:00:00');
INSERT INTO `bill` VALUES ('tom', '零食', '买鸡腿', '8', '2022-06-12 00:00:00');
INSERT INTO `bill` VALUES ('tom', '娱乐', '去游乐园', '199', '2022-06-12 00:00:00');
INSERT INTO `bill` VALUES ('tom', '交通出行', '打车', '11', '2022-06-12 00:00:00');
INSERT INTO `bill` VALUES ('tom', '生活缴费', '交电费', '120', '2022-06-12 00:00:00');
INSERT INTO `bill` VALUES ('tom', '网上购物', '买衣服', '58', '2022-06-12 00:00:00');
INSERT INTO `bill` VALUES ('tom', '其他', '买感冒药', '20', '2022-06-12 00:00:00');
INSERT INTO `bill` VALUES ('tom', '生活缴费', '网费', '750', '2022-06-12 00:00:00');
INSERT INTO `bill` VALUES ('tom', '伙食', '午饭', '12', '2022-06-12 00:00:00');
INSERT INTO `bill` VALUES ('tom', '伙食', '11', '11', '2022-06-16 00:00:00');

-- ----------------------------
-- Table structure for diary
-- ----------------------------
DROP TABLE IF EXISTS `diary`;
CREATE TABLE `diary`  (
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `content` varchar(3000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `place` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `weather` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of diary
-- ----------------------------
INSERT INTO `diary` VALUES ('摆烂啦', '今天彻底摆烂了', '2022-06-11 16:51:29', '111', '萍乡市', '小雨');
INSERT INTO `diary` VALUES ('111', '111', '2022-06-11 23:41:40', '111', '萍乡市', '小雨');
INSERT INTO `diary` VALUES ('111', '111', '2022-06-12 14:04:56', '111', '萍乡市', '小雨');
INSERT INTO `diary` VALUES ('快乐的一天', '今天，我去横店玩。我在那边骑了一趟马。吃了一个冰淇淋。看了电影《英雄》的片断。还在那边看了暴雨山洪呢。我们看暴雨山洪的时候，当洪水从山上冲下来时，在下面观看的那些人都不得不逃了。', '2022-06-12 20:30:22', 'tom', '萍乡市', '小雨');
INSERT INTO `diary` VALUES ('美好的一天', '早晨的太阳刚刚升起，美好的一天也就开始了。\n　　一缕阳光明亮地照耀着我的家，我边舒展筋骨边走出家门看看风景。小草上躺着珍珠似的露珠，一阵风吹过，小草像洗了把脸似的，渐渐清醒了，它伸了伸懒腰，挺直了身板。花儿们展开了花苞，慢慢地也清醒了。过了一会儿，太阳升高，花儿们展开了花瓣，露出笑容，灿烂无比。鸟儿们也都“起床”了，有的在吱吱喳喳地唱歌，有的在梳头发，还有的在觅食。多么美丽、富有生机的清晨啊!', '2022-06-12 21:01:52', 'tom', '萍乡市', '小雨');
INSERT INTO `diary` VALUES ('单独在家', '今天，爸爸妈妈要出去有事情，临走的时候，爸爸严厉地说：“小雨，你在家好好做作业。我回来的时候，你要写好一篇短文。”\n家里只剩下孤零零的我，一个人在家的味道真不好。我想来想去也不知道该怎么写，又担心爸爸回来的时候，如果我还没有写好，爸爸会骂我，越想越害怕，想着想着，我不由自主地拿起了电话，电话里立即传来了爸爸老虎般的声音：“小雨，干吗打电话给我\n是不是作文写好了\n还是作文不会写了\n如果作文还没有写好，回来我找你算帐。”我吓得大哭起来，爸爸又说：“不准哭，再哭我回来把你踩到地下面去。”', '2022-06-12 21:02:21', 'tom', '萍乡市', '小雨');
INSERT INTO `diary` VALUES ('出游记', '我们一家人去大纵湖玩。\n　　来到大纵湖旅游度假区里，首先看到一只芦苇做的船，叫古芦舟。我登上船，低头看见水里游着一群小鱼。它们有各种各样的色彩，有花的、有红的、还有金黄色的。\n　　接着，我们坐上小船进入了芦苇迷宫。小船后面的波浪把芦苇打得摇摇晃晃。湖边的树上有个喜鹊窝，小喜鹊在窝里发出“喳喳”的叫声。小船带着我们七拐八拐，终于回到了出发的地点。\n　　今天我玩得太开心了！', '2022-06-12 21:03:13', 'tom', '萍乡市', '小雨');
INSERT INTO `diary` VALUES ('我学会了蒸鸡蛋', '同学们，你们会做菜吗？肯定不会吧。今天，妈妈叫我做了一道菜——蒸蛋汤。\n　　我们先拿出一个生鸡蛋，把它的中间部分敲碎，把蛋壳分别往两边弄。让蛋掉到碗里，再往碗里灌半碗的自来水。然后，拿出一双筷子，用旋转的方式将蛋打开来，煮好了，我吃得津津有味。开心极了！', '2022-06-12 21:03:51', 'tom', '萍乡市', '小雨');
INSERT INTO `diary` VALUES ('不想长大', '最\n近一直过的好郁闷，说不出什么感觉，也道不明为什么郁闷！人活着很烦，工作，生活方方面面……总觉得自己好没用，可能是因为从小就没受到过什么挫折，先在\n长大了，（哦更准确的说自己在慢慢老去）感觉自己好脆弱，轻轻一碰就要碎了似的！每天漫无目的的活着，不想长大………', '2022-06-12 21:04:12', 'tom', '萍乡市', '小雨');
INSERT INTO `diary` VALUES ('11111', '11111', '2022-08-20 20:21:01', '111', '抚州市', '晴');

-- ----------------------------
-- Table structure for remind
-- ----------------------------
DROP TABLE IF EXISTS `remind`;
CREATE TABLE `remind`  (
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `setTime` datetime(0) NOT NULL,
  `remindTime` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `condi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of remind
-- ----------------------------
INSERT INTO `remind` VALUES ('tom', '记得', '2022-06-12 21:06:57', '21:08', '不记得', 'True');
INSERT INTO `remind` VALUES ('tom', '1111', '2022-06-12 21:11:17', '21:13', '11111', 'True');
INSERT INTO `remind` VALUES ('tom', '11111', '2022-06-13 08:22:01', '8:23', '11111', 'True');
INSERT INTO `remind` VALUES ('tom', '222', '2022-06-13 08:30:54', '08:32', '222', 'True');
INSERT INTO `remind` VALUES ('tom', '111', '2022-06-13 08:32:58', '08:34', '111', 'True');
INSERT INTO `remind` VALUES ('tom', '11', '2022-06-13 09:38:43', '09:40', '111', 'True');
INSERT INTO `remind` VALUES ('tom', '11', '2022-06-15 15:28:34', '15:30', '1', 'True');
INSERT INTO `remind` VALUES ('111', '扫地', '2022-08-20 20:38:45', '20:40', '记得把家里打扫一遍', 'True');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`username`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('11', '11');
INSERT INTO `user` VALUES ('111', '111');
INSERT INTO `user` VALUES ('1111', '111');
INSERT INTO `user` VALUES ('12', '12');
INSERT INTO `user` VALUES ('tom', '123456');

SET FOREIGN_KEY_CHECKS = 1;
