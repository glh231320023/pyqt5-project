# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'diary.ui'
#
# Created by: PyQt5 UI code generator 5.15.6
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow_diary(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setFixedSize(1275, 797)
        MainWindow.setStyleSheet("QWidget#centralwidget{\n"
"    \n"
"background-image: url(:/新前缀/diary.png);\n"
"}\n"
"\n"
"")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(50, 20, 1181, 55))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.pushButton_home = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        font = QtGui.QFont()
        font.setFamily("方正舒体")
        font.setPointSize(25)
        self.pushButton_home.setFont(font)
        self.pushButton_home.setObjectName("pushButton_home")
        self.horizontalLayout.addWidget(self.pushButton_home)
        self.pushButton_diary = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        font = QtGui.QFont()
        font.setFamily("方正舒体")
        font.setPointSize(25)
        self.pushButton_diary.setFont(font)
        self.pushButton_diary.setObjectName("pushButton_diary")
        self.horizontalLayout.addWidget(self.pushButton_diary)
        self.pushButton_bill = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        font = QtGui.QFont()
        font.setFamily("方正舒体")
        font.setPointSize(25)
        self.pushButton_bill.setFont(font)
        self.pushButton_bill.setObjectName("pushButton_bill")
        self.horizontalLayout.addWidget(self.pushButton_bill)
        self.pushButton_remind = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        font = QtGui.QFont()
        font.setFamily("方正舒体")
        font.setPointSize(25)
        self.pushButton_remind.setFont(font)
        self.pushButton_remind.setObjectName("pushButton_remind")
        self.horizontalLayout.addWidget(self.pushButton_remind)
        self.pushButton_exit = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        font = QtGui.QFont()
        font.setFamily("方正舒体")
        font.setPointSize(25)
        self.pushButton_exit.setFont(font)
        self.pushButton_exit.setObjectName("pushButton_exit")
        self.horizontalLayout.addWidget(self.pushButton_exit)
        self.tableWidget_diary = QtWidgets.QTableWidget(self.centralwidget)
        self.tableWidget_diary.setGeometry(QtCore.QRect(60, 130, 1151, 551))
        self.tableWidget_diary.setObjectName("tableWidget_diary")
        self.tableWidget_diary.setColumnCount(4)
        self.tableWidget_diary.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setFamily("方正舒体")
        font.setPointSize(25)
        item.setFont(font)
        self.tableWidget_diary.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setFamily("方正舒体")
        font.setPointSize(25)
        item.setFont(font)
        self.tableWidget_diary.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setFamily("方正舒体")
        font.setPointSize(25)
        item.setFont(font)
        self.tableWidget_diary.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setFamily("方正舒体")
        font.setPointSize(25)
        item.setFont(font)
        self.tableWidget_diary.setHorizontalHeaderItem(3, item)
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(60, 690, 171, 51))
        font = QtGui.QFont()
        font.setFamily("方正舒体")
        font.setPointSize(25)
        self.pushButton.setFont(font)
        self.pushButton.setObjectName("pushButton")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.pushButton_home.setText(_translate("MainWindow", "首页"))
        self.pushButton_diary.setText(_translate("MainWindow", "日记"))
        self.pushButton_bill.setText(_translate("MainWindow", "手账"))
        self.pushButton_remind.setText(_translate("MainWindow", "提醒事项"))
        self.pushButton_exit.setText(_translate("MainWindow", "退出"))
        item = self.tableWidget_diary.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "标题"))
        item = self.tableWidget_diary.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "时间"))
        item = self.tableWidget_diary.horizontalHeaderItem(2)
        item.setText(_translate("MainWindow", "操作一"))
        item = self.tableWidget_diary.horizontalHeaderItem(3)
        item.setText(_translate("MainWindow", "操作二"))
        self.pushButton.setText(_translate("MainWindow", "添加日记"))

    def beforeuse(self):
        self.pushButton_home.setStyleSheet(
            '''QPushButton{background:#8FBC8F;border-radius:10px;}QPushButton:hover{background:#FFFFFF;
            border-radius:10px;}
             QPushButton:checked{background:#90EE90;border-radius:10px;''')
        self.pushButton_diary.setStyleSheet(
            '''QPushButton{background:#90EE90;border-radius:10px;}QPushButton:hover{background:#FFFFFF;
            border-radius:10px;}
             QPushButton:checked{background:#90EE90;border-radius:10px;''')
        self.pushButton_bill.setStyleSheet(
            '''QPushButton{background:#8FBC8F;border-radius:10px;}QPushButton:hover{background:#FFFFFF;
            border-radius:10px;}
             QPushButton:checked{background:#90EE90;border-radius:10px;''')
        self.pushButton_remind.setStyleSheet(
            '''QPushButton{background:#8FBC8F;border-radius:10px;}QPushButton:hover{background:#FFFFFF;
            border-radius:10px;}
             QPushButton:checked{background:#90EE90;border-radius:10px;''')
        self.pushButton_exit.setStyleSheet(
            '''QPushButton{background:#8FBC8F;border-radius:10px;}QPushButton:hover{background:#FFFFFF;
            border-radius:10px;}
             QPushButton:checked{background:#90EE90;border-radius:10px;''')
        self.pushButton.setStyleSheet(
            '''QPushButton{background:#8FBC8F;border-radius:10px;}QPushButton:hover{background:#FFFFFF;
            border-radius:10px;}
             QPushButton:checked{background:#90EE90;border-radius:10px;''')
