from datetime import datetime
from pyecharts import options as opts
from pyecharts.charts import Pie
from pyecharts.faker import Faker
import pymysql
import requests
from PyQt5.QtCore import Qt, QUrl
from PyQt5.QtCore import QTimer, QDate, QDateTime, QTime
from PyQt5.QtGui import QPalette, QPixmap, QBrush
from PyQt5.QtWebEngineWidgets import QWebEngineView
from PyQt5.QtWidgets import QApplication, QWidget, QDialog, QMessageBox, QHeaderView, QAbstractItemView, \
    QTableWidgetItem, QPushButton, QHBoxLayout, QMainWindow, QGridLayout, QDateTimeEdit, QFrame

from add_bill import Ui_MainWindow_add_bill
from add_diary import Ui_MainWindow_add_diary
from add_matter import Ui_MainWindow_add_matter
from bill import Ui_MainWindow_bill
from diary import Ui_MainWindow_diary
from historical_event import Ui_MainWindow_history_event
from login import Ui_MainWindow_login
from modify_matter import Ui_MainWindow_modify_matter
from register import Ui_MainWindow_register
from home import Ui_MainWindow_home
from remind import Ui_MainWindow_remind
from screen_bill import Ui_MainWindow_screen_bill
from see_diary import Ui_MainWindow_see_diary
from see_matter import Ui_MainWindow_see_matter
from web import Ui_Form

global username
username = 'user'


class mysql(object):
    def __init__(self):
        pass

    def connect(self, s):
        try:
            conn = pymysql.connect(host='127.0.0.1' # 连接名称,默认127.0.0.1
                                   , user='root' # 用户名
                                   , passwd='root'  # 密码
                                   , port=3306  # 端口，默认为3306
                                   , db='notepad'  # 数据库名称
                                   , charset='utf8'  # 字符编码
                                   )
            cur = conn.cursor()  # 生成游标对象
            sql = s  # SQL语句
            cur.execute(sql)  # 执行SQL语句
            conn.commit()
            data = cur.fetchall()  # data是返回查询结果
            a = cur.rowcount        #a是返回sql语句影响的条数
            cur.close()  # 关闭游标
            conn.close()  # 关闭连接
        except Exception as e:
            print(e)
            msg_box = QMessageBox(QMessageBox.Warning, '警告', '数据连接失败!')
            msg_box.exec_()
        return data, a


class login(QMainWindow, Ui_MainWindow_login):
    """docstring for Cacular"""
    def __init__(self):
        super(login, self).__init__()
        self.setupUi(self)
        self.show()
        self.beforeuse()
        self.use()

    def ps_pushButton(self):
        text1 = self.lineEdit.text()
        text2 = self.lineEdit_2.text()
        sql = mysql()
        s = 'select * from user'
        data = sql.connect(s)[0]
        i = 0
        while i < len(data):
            if text1 == data[i][0] and text2 == data[i][1]:
                global username
                username = text1
                self.close()
                self.Jump1=home()
                msg_box = QMessageBox(QMessageBox.Information, '提示', '登入成功!')
                msg_box.exec_()
                break
            elif i == len(data) - 1:
                msg_box = QMessageBox(QMessageBox.Critical, '错误', '用户名或密码错误!')
                msg_box.exec_()
            i = i + 1

    def ps_pushButton_2(self):
        self.close()
        self.Jump = register()


    def use(self):
        self.pushButton.clicked.connect(self.ps_pushButton)
        self.pushButton_2.clicked.connect(self.ps_pushButton_2)

class register(QMainWindow, Ui_MainWindow_register):
    def __init__(self):
        super(register, self).__init__()
        self.setupUi(self)
        self.show()
        self.beforeuse()
        self.use()

    def ps_pushButton(self):
        text1 = self.lineEdit.text()
        text2 = self.lineEdit_2.text()
        text3 = self.lineEdit_3.text()
        sql = mysql()
        s = "insert into user (username,password) values ('" + text1 + "','" + text2 + "')"
        s1 = "select username from user where username='"+text1+"'"
        a = sql.connect(s1)[1]
        if text1 == '' and text2 == '':
            msg_box = QMessageBox(QMessageBox.Critical, '错误', '用户名或密码为空!')
            msg_box.exec_()
        else:
            if a == 1:
                msg_box = QMessageBox(QMessageBox.Critical, '错误', '用户名已存在!')
                msg_box.exec_()
            else:
                if text2 != text3:
                    msg_box = QMessageBox(QMessageBox.Critical, '错误', '两次输入的密码不一致!')
                    msg_box.exec_()
                else:
                    a1=sql.connect(s)[1]
                    if a1==1:
                        self.close()
                        self.Jump=login()
                        msg_box = QMessageBox(QMessageBox.Information, '提示', '注册成功!')
                        msg_box.exec_()
                    else:
                        msg_box = QMessageBox(QMessageBox.Critical, '错误', '注册失败!')
                        msg_box.exec_()

    def use(self):
        self.pushButton.clicked.connect(self.ps_pushButton)


class weather(object):
    def __init__(self):
        pass
    def obtain(self):
        url = 'http://whois.pconline.com.cn/ipJson.jsp'
        ip = requests.get('http://ifconfig.me/ip').text.strip()    #获取IP地址
        param = {'ip': ip,
                 'json': 'true'
                 }
        ree = requests.get(url, params=param)
        ip_city=ree.json()
        city = ip_city['city']    #根据ip调用接口获取城市名
        yburl = 'https://devapi.qweather.com/v7/weather/3d?'  # 获取3天的天气
        cyurl = 'https://devapi.qweather.com/v7/indices/1d?type=3'  # 获取推荐
        url = 'https://geoapi.qweather.com/v2/city/lookup?'  # 获取地区id
        value1 = {
            'location': city,
            'key': 'd2bd54bdec2e41ca8a875e58b9531dc8',
            'lang': 'zh'
        }
        id = requests.get(url, params=value1)
        data = id.json()
        value = {
            'location': data['location'][0]['id'],
            'key': 'd2bd54bdec2e41ca8a875e58b9531dc8',
            'lang': 'zh'
        }
        cy = requests.get(cyurl, params=value)
        yb = requests.get(yburl, params=value)
        life = cy.json()
        weater = yb.json()
        return life,weater,ip_city   #ip_city返回ip信息的json格式,life是返回生活推荐指数的json格式weater是返回3天天气的json查看字段说明可访问https://dev.qweather.com/docs/api/


class yiyan(object):
    def __init__(self):
        pass

    def geturl(self):
        yiyan = requests.get("http://v1.hitokoto.cn")
        yiyan.encoding = "utf-8"
        text = yiyan.json()
        '''print("文学句:{}".format(yiyan.json()["hitokoto"]))
        print("出处:{}".format(yiyan.json()["from"]))
        print("作者:{}".format(yiyan.json()["from_who"]))'''
        return text         # text返回一言json格式使用方法https://developer.hitokoto.cn/sentence/#%E7%AE%80%E4%BB%8B


class home(QMainWindow,Ui_MainWindow_home):
    def __init__(self):
        super(home, self).__init__()
        self.setupUi(self)
        self.show()
        self.ps_settext()
        self.setyiyan()
        self.settime()
        self.setText()
        self.beforeuse()
        self.use()

    def ps_settext(self):
        Wt=weather().obtain()
        city=Wt[2]['pro']+Wt[2]['city']
        weat=Wt[1]['daily'][0]['textDay']
        global weater_all, ip_city_all
        weater_all = weat
        ip_city_all = Wt[2]['city']
        temperature=Wt[1]['daily'][0]['tempMin']+'℃-'+Wt[1]['daily'][0]['tempMax']+'℃'
        humidity=Wt[1]['daily'][0]['humidity']
        recommend=Wt[0]['daily'][0]['text']
        self.label_city.setText(city)
        self.label_weather.setText(weat)
        self.label_temperature.setText(temperature)
        self.label_humidity.setText(humidity)
        self.label_recommend.setWordWrap(True)
        self.label_recommend.setText(recommend)

    def setyiyan(self):
            yiyan_json=yiyan().geturl()
            text =yiyan_json['hitokoto']
            author = '----' + yiyan_json['from']
            self.label_yiyan.setWordWrap(True)
            self.label_yiyan.setText(text)
            self.label_author.setText(author)


    #设置10秒自动更新一言语句
    def settime(self):
        self.timer_yiyan = QTimer()
        self.timer_yiyan.timeout.connect(self.setyiyan)  # 计时器挂接到槽：update
        self.timer_yiyan.start(10000)

    def setText(self):
        layout = QGridLayout()
        self.timer = QTimer()
        self.timer.timeout.connect(self.showTime)
        layout.addWidget(self.label_time, 0, 0, 1, 2)
        self.setLayout(layout)
        self.timer.start(1000)

    def showTime(self):
        now = QDate.currentDate()  # 获取当前日期
        datetime = QDateTime.currentDateTime()  # 获取当前日期与时间
        time = QTime.currentTime()  # 获取当前时间
        timeDisplay = (now.toString(Qt.ISODate) + ' ' + datetime.toString()[:2] + ' ' + time.toString(
            Qt.DefaultLocaleLongDate))
        self.label_time.setText(timeDisplay)

    def ps_Button_home(self):
        self.close()
        self.Jump = home()


    def ps_Button_diary(self):
        self.close()
        self.Jump = diary()


    def ps_Button_bill(self):
        self.close()
        self.Jump = bill()


    def ps_Button_remind(self):
        self.close()
        self.Jump = remind()


    def ps_Button_exit(self):
        self.close()
        self.Jump = login()

    def use(self):
        self.pushButton_home.clicked.connect(self.ps_Button_home)
        self.pushButton_diary.clicked.connect(self.ps_Button_diary)
        self.pushButton_bill.clicked.connect(self.ps_Button_bill)
        self.pushButton_remind.clicked.connect(self.ps_Button_remind)
        self.pushButton_exit.clicked.connect(self.ps_Button_exit)
        self.Jump = remindTips()

class diary(QMainWindow,Ui_MainWindow_diary):

    def __init__(self):
        super(diary, self).__init__()
        self.setupUi(self)
        self.show()
        self.beforeuse()
        self.use()
        self.table()

    def ps_Button_home(self):
        self.close()
        self.Jump = home()


    def ps_Button_diary(self):
        self.close()
        self.Jump = diary()


    def ps_Button_bill(self):
        self.close()
        self.Jump = bill()


    def ps_Button_remind(self):
        self.close()
        self.Jump = remind()


    def ps_Button_exit(self):
        self.close()
        self.Jump = login()


    def ps_Button_add(self):
        self.Jump = add_diary()


    def selectbutton(self, titl,time):

        widget = QWidget()
        # 查看详情
        updateBtn = QPushButton('查看详情')
        updateBtn.setStyleSheet(''' text-align : center;
                                           background-color : DarkSeaGreen;
                                           height : 30px;
                                           border-style: outset;
                                              font : 13px  ''')
        updateBtn.clicked.connect(lambda: self.selectTable(titl,time))
        hLayout = QHBoxLayout()
        hLayout.addWidget(updateBtn)
        hLayout.setContentsMargins(5, 2, 5, 2)
        widget.setLayout(hLayout)
        return widget
    def deletebutton(self,titl,time):
        widget = QWidget()
        # 删除
        deleteBtn = QPushButton('删除')
        deleteBtn.setStyleSheet(''' text-align : center;
                                                           background-color : red;
                                                           height : 30px;
                                                           border-style: outset;
                                                           font : 13px  ''')
        deleteBtn.clicked.connect(lambda: self.deleteTable(titl,time))
        hLayout = QHBoxLayout()
        hLayout.addWidget(deleteBtn)
        hLayout.setContentsMargins(5, 2, 5, 2)
        widget.setLayout(hLayout)
        return widget

    def selectTable(self,titl,time):
        global title,Time
        title=titl
        Time=time
        self.Jump=see_diary()

    def deleteTable(self,titl,time):
        global title,username,Time
        title=titl
        Time=time
        sql=mysql()
        s="delete from diary where username='"+username+"' and title='"+title+"' and time='"+Time+"'"
        a=sql.connect(s)[1]
        if a==1:
            self.close()
            self.Jump=diary()
            msg_box = QMessageBox(QMessageBox.Information, '提示', '删除成功!')
            msg_box.exec_()
        else:
            msg_box = QMessageBox(QMessageBox.Critical, '错误', '删除失败!')
            msg_box.exec_()

    def table(self):
        global username
        #self.tableWidget_diary.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)   #让表格铺满整个QTableWidget控件。
        self.tableWidget_diary.setEditTriggers(QAbstractItemView.NoEditTriggers)   #将表格变为禁止编辑。
        self.tableWidget_diary.horizontalHeader().resizeSection(0, 400)
        self.tableWidget_diary.horizontalHeader().resizeSection(1, 250)
        self.tableWidget_diary.horizontalHeader().resizeSection(2, 240)
        self.tableWidget_diary.horizontalHeader().resizeSection(3, 240)
        # self.tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch) 一定要注释，否则上述设置不生效
        sql=mysql()
        s="select title,time from diary where username='"+username+"'"
        items = sql.connect(s)[0]
        for i in range(len(items)):
            item = items[i]
            row = self.tableWidget_diary.rowCount()
            self.tableWidget_diary.insertRow(row)
            for j in range(len(item)):
                item = QTableWidgetItem(str(items[i][j]))
                self.tableWidget_diary.setItem(row, j, item)
                self.tableWidget_diary.setCellWidget(row,2,self.deletebutton(str(items[i][0]),str(items[i][1])))
                self.tableWidget_diary.setCellWidget(row, 3, self.selectbutton(str(items[i][0]),str(items[i][1])))

    def use(self):
        self.pushButton_home.clicked.connect(self.ps_Button_home)
        self.pushButton_diary.clicked.connect(self.ps_Button_diary)
        self.pushButton_bill.clicked.connect(self.ps_Button_bill)
        self.pushButton_remind.clicked.connect(self.ps_Button_remind)
        self.pushButton_exit.clicked.connect(self.ps_Button_exit)
        self.pushButton.clicked.connect(self.ps_Button_add)
        self.Jump=remindTips()

class add_diary(QMainWindow,Ui_MainWindow_add_diary):

    def __init__(self):
        super(add_diary, self).__init__()
        self.setupUi(self)
        self.show()
        self.use()
        self.beforuse()

    def ps_Button(self):
        global username,weater_all,ip_city_all
        text1 = self.lineEdit_title.text()
        text2 = self.plainTextEdit.toPlainText()
        sql = mysql()
        s = "insert into diary (title,content,username,place,weather) values ('" + text1 + "','" + text2 + "','" + username + "','"+ip_city_all+"','"+weater_all+"')"
        if text1 == '' and text2 == '':
            msg_box = QMessageBox(QMessageBox.Critical, '错误', '标题或内容为空!')
            msg_box.exec_()
        else:
            a = sql.connect(s)[1]
            if a == 1:
                self.close()
                msg_box = QMessageBox(QMessageBox.Information, '提示', '添加成功!点击页面即可刷新')
                msg_box.exec_()
            else:
                msg_box = QMessageBox(QMessageBox.Critical, '错误', '添加失败!')
                msg_box.exec_()

    def use(self):
        global weater_all, ip_city_all
        self.pushButton.clicked.connect(self.ps_Button)
        self.label_place.setText(ip_city_all)
        self.label_weather.setText(weater_all)


class see_diary(QMainWindow,Ui_MainWindow_see_diary):

    def __init__(self):
        super(see_diary, self).__init__()
        self.setupUi(self)
        self.show()
        self.see_text()
    def see_text(self):
        global title,username,Time
        sql=mysql()
        s="select title,content,time,place,weather from diary where username='"+username+"' and title='"+title+"' and time='"+Time+"'"
        data=sql.connect(s)[0]
        self.label_title.setText(data[0][0])
        self.label_time.setText(str(data[0][2].date()))
        self.label_place.setText(data[0][3])
        self.label_weather.setText(data[0][4])
        self.plainTextEdit_text.setPlainText(data[0][1])


class bill(QMainWindow,Ui_MainWindow_bill):

    def __init__(self):
        super(bill, self).__init__()
        self.setupUi(self)
        self.show()
        self.beforuse()
        self.use()
        self.table()

    def ps_Button_home(self):
        self.close()
        self.Jump = home()


    def ps_Button_diary(self):
        self.close()
        self.Jump = diary()


    def ps_Button_bill(self):
        self.close()
        self.Jump = bill()


    def ps_Button_remind(self):
        self.close()
        self.Jump = remind()


    def ps_Button_exit(self):
        self.close()
        self.Jump = login()


    def ps_Button_add(self):
        self.Jump = add_bill()


    def ps_Button_2(self):
        self.Jump = screen_bill()

    def deletebutton(self,type,money,time):
        widget = QWidget()
        # 删除
        deleteBtn = QPushButton('删除')
        deleteBtn.setStyleSheet(''' text-align : center;
                                                           background-color : red;
                                                           height : 30px;
                                                           border-style: outset;
                                                           font : 13px  ''')
        deleteBtn.clicked.connect(lambda: self.deleteTable(type,money,time))
        hLayout = QHBoxLayout()
        hLayout.addWidget(deleteBtn)
        hLayout.setContentsMargins(5, 2, 5, 2)
        widget.setLayout(hLayout)
        return widget

    def deleteTable(self,type,money,time):
        global type_bill,username,time_bill
        type_bill=type
        time_bill=time
        sql=mysql()
        s="delete from bill where username='"+username+"' and type='"+type_bill+"' and time='"+time_bill+"' and money='"+money+"'"
        a=sql.connect(s)[1]
        if a==1:
            self.close()
            self.Jump=bill()
            msg_box = QMessageBox(QMessageBox.Information, '提示', '删除成功!')
            msg_box.exec_()
        else:
            msg_box = QMessageBox(QMessageBox.Critical, '错误', '删除失败!')
            msg_box.exec_()

    def table(self):
        global username
        #self.tableWidget_diary.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)   #让表格铺满整个QTableWidget控件。
        self.tableWidget.setEditTriggers(QAbstractItemView.NoEditTriggers)   #将表格变为禁止编辑。
        self.tableWidget.horizontalHeader().resizeSection(0, 210)
        self.tableWidget.horizontalHeader().resizeSection(1, 200)
        self.tableWidget.horizontalHeader().resizeSection(2, 300)
        self.tableWidget.horizontalHeader().resizeSection(3, 200)
        self.tableWidget.horizontalHeader().resizeSection(4, 200)
        # self.tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch) 一定要注释，否则上述设置不生效
        # self.tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch) 一定要注释，否则上述设置不生效
        sql = mysql()
        s = "select time,type,utility,money from bill where username='" + username + "'"
        items = sql.connect(s)[0]
        for i in range(len(items)):
            item = items[i]
            row = self.tableWidget.rowCount()
            self.tableWidget.insertRow(row)
            for j in range(len(item)):
                item = QTableWidgetItem(str(items[i][j]))
                self.tableWidget.setItem(row, j, item)
                self.tableWidget.setCellWidget(row, 4, self.deletebutton(str(items[i][1]), str(items[i][3]),str(items[i][0])))

    def use(self):
        self.pushButton_home.clicked.connect(self.ps_Button_home)
        self.pushButton_diary.clicked.connect(self.ps_Button_diary)
        self.pushButton_bill.clicked.connect(self.ps_Button_bill)
        self.pushButton_remind.clicked.connect(self.ps_Button_remind)
        self.pushButton_exit.clicked.connect(self.ps_Button_exit)
        self.pushButton.clicked.connect(self.ps_Button_add)
        self.pushButton_2.clicked.connect(self.ps_Button_2)
        self.Jump=remindTips()


class add_bill(QMainWindow,Ui_MainWindow_add_bill):

    def __init__(self):
        super(add_bill, self).__init__()
        self.setupUi(self)
        self.show()
        self.beforuse()
        self.use()

    def ps_Button(self):
        global username
        time=self.calendarWidget.selectedDate().toString("yyyy-MM-dd")
        type=self.comboBox.currentText()
        utility=self.lineEdit_Utility.text()
        money=self.lineEdit_money.text()
        time_bill=str(datetime.strptime(time, '%Y-%m-%d'))
        s = "insert into bill values ('" + username + "','" + type + "','" + utility + "','" + money + "','" + time_bill + "')"
        sql=mysql()
        if utility=='' and money=='':
            msg_box = QMessageBox(QMessageBox.Critical, '错误', '具体用处或金额为空!')
            msg_box.exec_()
        else:
            a=sql.connect(s)[1]
            if a==1:
                self.close()
                msg_box = QMessageBox(QMessageBox.Information, '提示', '添加成功!点击页面刷新')
                msg_box.exec_()
            else:
                msg_box = QMessageBox(QMessageBox.Critical, '错误', '删除失败!')
                msg_box.exec_()

    def use(self):
        self.pushButton.clicked.connect(self.ps_Button)


class screen_bill(QMainWindow,Ui_MainWindow_screen_bill):

    def __init__(self):
        super(screen_bill, self).__init__()
        self.setupUi(self)
        self.show()
        self.beforuse()
        self.use()

    def ps_Button(self):
        global username
        time1=self.dateEdit.date().toString("yyyy-MM-dd")
        time1_bill=str(datetime.strptime(time1, '%Y-%m-%d'))
        time2=self.dateEdit_2.date().toString("yyyy-MM-dd")
        time2_bill=str(datetime.strptime(time2, '%Y-%m-%d'))
        type = self.comboBox.currentText()
        sql = mysql()
        s="select time,type,utility,money from bill where username='"+username+"' and time between'"+time1_bill+"' and '"+time2_bill+"'"
        s_typ="select time,type,utility,money from bill where username='"+username+"' and type='"+type+"' and time between'"+time1_bill+"'and'"+time2_bill+"'"
        self.tableWidget.setRowCount(0)
        self.tableWidget.clearContents()
        if type == '全部':
            self.tableWidget.setRowCount(0)
            self.tableWidget.clearContents()
            items = sql.connect(s)[0]
            a=sql.connect(s)[1]
            if a ==0:
                msg_box = QMessageBox(QMessageBox.Information, '提示', '暂无记录!')
                msg_box.exec_()
            for i in range(len(items)):
                item = items[i]
                row = self.tableWidget.rowCount()
                self.tableWidget.insertRow(row)
                for j in range(len(item)):
                    item = QTableWidgetItem(str(items[i][j]))
                    self.tableWidget.setItem(row, j, item)
            a=0
            for i in range(len(items)):
                a=a+eval(items[i][3])
            money_bill=str(a)+'元'
            self.label_5.setText(money_bill)
        else:
            self.tableWidget.setRowCount(0)
            self.tableWidget.clearContents()
            items = sql.connect(s_typ)[0]
            a = sql.connect(s_typ)[1]
            if a == 0:
                msg_box = QMessageBox(QMessageBox.Information, '提示', '暂无记录!')
                msg_box.exec_()
            for i in range(len(items)):
                item = items[i]
                row = self.tableWidget.rowCount()
                self.tableWidget.insertRow(row)
                for j in range(len(item)):
                    item = QTableWidgetItem(str(items[i][j]))
                    self.tableWidget.setItem(row, j, item)
            a = 0
            for i in range(len(items)):
                a = a + eval(items[i][3])
            money_bill = str(a) + '元'
            self.label_5.setText(money_bill)
    def ps_pushButton_Statistics(self):
        global username
        time1 = self.dateEdit.date().toString("yyyy-MM-dd")
        time1_bill = str(datetime.strptime(time1, '%Y-%m-%d'))
        time2 = self.dateEdit_2.date().toString("yyyy-MM-dd")
        time2_bill = str(datetime.strptime(time2, '%Y-%m-%d'))
        s = "select type,sum(money) from bill where username='" + username + "' and time between'" + time1_bill + "' and '" + time2_bill + "' group by type"
        sql=mysql()
        data=sql.connect(s)[0]
        titll=time1+'到'+time2+'消费统计图'
        if sql.connect(s)[1]!=0:
            self.Jump=web(data,titll)
        else:
            msg_box = QMessageBox(QMessageBox.Information, '提示', '暂无记录!')
            msg_box.exec_()
    def use(self):
        self.pushButton.clicked.connect(self.ps_Button)
        self.pushButton_Statistics.clicked.connect(self.ps_pushButton_Statistics)
        self.tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)  # 让表格铺满整个QTableWidget控件。
        self.tableWidget.setEditTriggers(QAbstractItemView.NoEditTriggers)  # 将表格变为禁止编辑。


class remind(QMainWindow,Ui_MainWindow_remind):

    def __init__(self):
        super(remind, self).__init__()
        self.setupUi(self)
        self.show()
        self.beforeuse()
        self.use()
        self.table()

    def selectbutton(self, tit, time):
        widget = QWidget()
        # 查看详情
        updateBtn = QPushButton('查看详情')
        updateBtn.setStyleSheet(''' text-align : center;
                                           background-color : DarkSeaGreen;
                                           height : 30px;
                                           border-style: outset;
                                              font : 13px  ''')
        updateBtn.clicked.connect(lambda: self.selectTable(tit, time))
        hLayout = QHBoxLayout()
        hLayout.addWidget(updateBtn)
        hLayout.setContentsMargins(5, 2, 5, 2)
        widget.setLayout(hLayout)
        return widget

    def deletebutton(self, tit,time):
        widget = QWidget()
        # 删除
        deleteBtn = QPushButton('删除')
        deleteBtn.setStyleSheet(''' text-align : center;
                                                           background-color : red;
                                                           height : 30px;
                                                           border-style: outset;
                                                           font : 13px  ''')
        deleteBtn.clicked.connect(lambda: self.deleteTable(tit, time))
        hLayout = QHBoxLayout()
        hLayout.addWidget(deleteBtn)
        hLayout.setContentsMargins(5, 2, 5, 2)
        widget.setLayout(hLayout)
        return widget

    def modifybutton(self, tit, time):
        widget = QWidget()
        # 更改
        modifyBtn = QPushButton('更改')
        modifyBtn.setStyleSheet(''' text-align : center;
                                                           background-color :DarkSeaGreen;
                                                           height : 30px;
                                                           border-style: outset;
                                                           font : 13px  ''')
        modifyBtn.clicked.connect(lambda: self.modifyTable(tit, time))
        hLayout = QHBoxLayout()
        hLayout.addWidget(modifyBtn)
        hLayout.setContentsMargins(5, 2, 5, 2)
        widget.setLayout(hLayout)
        return widget

    def selectTable(self, tit, time):
        global title,Time
        title = tit
        Time = time
        self.Jump = see_matter()

    def deleteTable(self, tit, time):
        global title, username,Time
        title = tit
        Time = time
        sql = mysql()
        s = "delete from remind where username='" + username + "' and title='" + title + "' and setTime='" + Time + "'"
        a = sql.connect(s)[1]
        if a == 1:
            self.close()
            self.Jump = remind()
            msg_box = QMessageBox(QMessageBox.Information, '提示', '删除成功!')
            msg_box.exec_()
        else:
            msg_box = QMessageBox(QMessageBox.Critical, '错误', '删除失败!')
            msg_box.exec_()

    def modifyTable(self, tit, time):
        global title, Time
        title = tit
        Time = time
        self.Jump = modify_matter()

    def table(self):
        global username
        # self.tableWidget_remind.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)   #让表格铺满整个QTableWidget控件。
        self.tableWidget_remind.setEditTriggers(QAbstractItemView.NoEditTriggers)  # 将表格变为禁止编辑。
        self.tableWidget_remind.horizontalHeader().resizeSection(0, 100)
        self.tableWidget_remind.horizontalHeader().resizeSection(1, 300)
        self.tableWidget_remind.horizontalHeader().resizeSection(2, 200)
        self.tableWidget_remind.horizontalHeader().resizeSection(3, 200)
        self.tableWidget_remind.horizontalHeader().resizeSection(4, 200)
        # self.tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch) 一定要注释，否则上述设置不生效
        sql = mysql()
        a="False"
        s = "select title,setTime,remindTime from remind where username='" + username + "' and condi='"+a+"'"
        items = sql.connect(s)[0]
        for i in range(len(items)):
            item = items[i]
            row = self.tableWidget_remind.rowCount()
            self.tableWidget_remind.insertRow(row)
            for j in range(len(item)):
                item = QTableWidgetItem(str(items[i][j]))
                self.tableWidget_remind.setItem(row, j, item)
                self.tableWidget_remind.setCellWidget(row, 3, self.deletebutton(str(items[i][0]), str(items[i][1])))
                self.tableWidget_remind.setCellWidget(row, 4, self.selectbutton(str(items[i][0]), str(items[i][1])))
                self.tableWidget_remind.setCellWidget(row, 5, self.modifybutton(str(items[i][0]), str(items[i][1])))

    def ps_Button_home(self):
        self.close()
        self.Jump = home()


    def ps_Button_diary(self):
        self.close()
        self.Jump = diary()


    def ps_Button_bill(self):
        self.close()
        self.Jump = bill()


    def ps_Button_remind(self):
        self.close()
        self.Jump = remind()


    def ps_Button_exit(self):
        self.close()
        self.Jump = login()


    def ps_Button_add(self):
        self.Jump = add_matter()


    def ps_Button_history(self):
        self.Jump = history_event()


    def use(self):
        self.pushButton_home.clicked.connect(self.ps_Button_home)
        self.pushButton_diary.clicked.connect(self.ps_Button_diary)
        self.pushButton_bill.clicked.connect(self.ps_Button_bill)
        self.pushButton_remind.clicked.connect(self.ps_Button_remind)
        self.pushButton_exit.clicked.connect(self.ps_Button_exit)
        self.pushButton_add.clicked.connect(self.ps_Button_add)
        self.pushButton_history.clicked.connect(self.ps_Button_history)
        self.Jump=remindTips()


class add_matter(QMainWindow,Ui_MainWindow_add_matter):

    def __init__(self):
        super(add_matter, self).__init__()
        self.setupUi(self)
        self.show()
        self.beforuse()
        self.use()

    def ps_Button_add(self):
        global username
        now = QDate.currentDate()  # 获取当前日期
        datetime = QDateTime.currentDateTime()  # 获取当前日期与时间
        time = QTime.currentTime()  # 获取当前时间
        SetTime = (now.toString(Qt.ISODate)+' '+time.toString(Qt.DefaultLocaleLongDate))
        nowtime=time.toString(Qt.DefaultLocaleLongDate).split(':')
        RemindTime = self.timeEdit.text()
        rem=RemindTime.split(':')
        if len(rem[0]) == 1:
            RemindTime1 = '0'+RemindTime
        else:
            RemindTime1 = RemindTime
        Title = self.lineEdit_title.text()
        Content = self.plainTextEdit_content.toPlainText()
        Condition = 'False'
        # print(SetTime,RemindTime,Content,Title,Condition,username)
        sql = mysql()
        s = "insert into remind values ('" + username + "','" + Title + "','" + SetTime + "','" + RemindTime1 + "','" + Content + "','" + Condition + "')"

        if int(nowtime[0]) == int(rem[0]):
            if int(nowtime[1]) <= int(rem[1]):
                if Title != '' and Content != '':
                    if sql.connect(s)[1] == 1:
                        self.close()
                        msg_box = QMessageBox(QMessageBox.Information, '提示', '添加成功!点击页面刷新')
                        msg_box.exec_()
                    else:
                        msg_box = QMessageBox(QMessageBox.Critical, '错误', '添加失败!')
                        msg_box.exec_()
                else:
                    msg_box = QMessageBox(QMessageBox.Critical, '错误', '标题或内容为空!')
                    msg_box.exec_()
            else:
                msg_box = QMessageBox(QMessageBox.Critical, '错误', '设置时间已经过去请重新设置一个!')
                msg_box.exec_()
        elif int(nowtime[0])<int(rem[0]):
            if Title != '' and Content != '':
                if sql.connect(s)[1] == 1:
                    self.close()
                    msg_box = QMessageBox(QMessageBox.Information, '提示', '添加成功!点击页面刷新')
                    msg_box.exec_()
                else:
                    msg_box = QMessageBox(QMessageBox.Critical, '错误', '添加失败!')
                    msg_box.exec_()
            else:
                msg_box = QMessageBox(QMessageBox.Critical, '错误', '标题或内容为空!')
                msg_box.exec_()
        else:
            msg_box = QMessageBox(QMessageBox.Critical, '错误', '设置时间已经过去请重新设置一个!')
            msg_box.exec_()

    def use(self):
        self.pushButton_add.clicked.connect(self.ps_Button_add)


class history_event(QMainWindow,Ui_MainWindow_history_event):

    def __init__(self):
        super(history_event, self).__init__()
        self.setupUi(self)
        self.show()
        self.beforuse()
        self.use()
        self.table()

    def selectbutton(self, tit, time):
        widget = QWidget()
        # 查看详情
        updateBtn = QPushButton('查看详情')
        updateBtn.setStyleSheet(''' text-align : center;
                                           background-color : DarkSeaGreen;
                                           height : 30px;
                                           border-style: outset;
                                              font : 13px  ''')
        updateBtn.clicked.connect(lambda: self.selectTable(tit, time))
        hLayout = QHBoxLayout()
        hLayout.addWidget(updateBtn)
        hLayout.setContentsMargins(5, 2, 5, 2)
        widget.setLayout(hLayout)
        return widget

    def deletebutton(self, tit,time):
        widget = QWidget()
        # 删除
        deleteBtn = QPushButton('删除')
        deleteBtn.setStyleSheet(''' text-align : center;
                                                           background-color : red;
                                                           height : 30px;
                                                           border-style: outset;
                                                           font : 13px  ''')
        deleteBtn.clicked.connect(lambda: self.deleteTable(tit, time))
        hLayout = QHBoxLayout()
        hLayout.addWidget(deleteBtn)
        hLayout.setContentsMargins(5, 2, 5, 2)
        widget.setLayout(hLayout)
        return widget

    def selectTable(self, tit, time):
        global title,Time
        title = tit
        Time = time
        self.Jump = see_matter()

    def deleteTable(self, tit, time):
        global title, username,Time
        title = tit
        Time = time
        sql = mysql()
        s = "delete from remind where username='" + username + "' and title='" + title + "' and setTime='" + Time + "'"
        a = sql.connect(s)[1]
        if a == 1:
            self.close()
            self.Jump = history_event()
            msg_box = QMessageBox(QMessageBox.Information, '提示', '删除成功!')
            msg_box.exec_()
        else:
            msg_box = QMessageBox(QMessageBox.Critical, '错误', '删除失败!')
            msg_box.exec_()

    def table(self):
        global username
        self.tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)   #让表格铺满整个QTableWidget控件。
        self.tableWidget.setEditTriggers(QAbstractItemView.NoEditTriggers)  # 将表格变为禁止编辑。
        # self.tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch) 一定要注释，否则上述设置不生效
        c="True"
        sql = mysql()
        s = "select title,setTime from remind where username='"+username+"' and condi='"+c+"'"
        items = sql.connect(s)[0]
        for i in range(len(items)):
            item = items[i]
            row = self.tableWidget.rowCount()
            self.tableWidget.insertRow(row)
            for j in range(len(item)):
                item = QTableWidgetItem(str(items[i][j]))
                self.tableWidget.setItem(row, j, item)
                self.tableWidget.setCellWidget(row, 2, self.deletebutton(str(items[i][0]), str(items[i][1])))
                self.tableWidget.setCellWidget(row, 3, self.selectbutton(str(items[i][0]), str(items[i][1])))

    def ps_Button(self):
        global username
        time1 = self.dateEdit.date().toString("yyyy-MM-dd")
        time2 = self.dateEdit_2.date().toString("yyyy-MM-dd")
        c='True'
        sql = mysql()
        s = "select title,setTime from remind where username='" + username + "' and condi='"+c+"' and setTime between'" + time1 + "' and '" + time2 + "'"
        self.tableWidget.setRowCount(0)
        self.tableWidget.clearContents()
        items = sql.connect(s)[0]
        a = sql.connect(s)[1]
        if a == 0:
            msg_box = QMessageBox(QMessageBox.Information, '提示', '暂无记录!')
            msg_box.exec_()
        for i in range(len(items)):
            item = items[i]
            row = self.tableWidget.rowCount()
            self.tableWidget.insertRow(row)
            for j in range(len(item)):
                item = QTableWidgetItem(str(items[i][j]))
                self.tableWidget.setItem(row, j, item)
                self.tableWidget.setCellWidget(row, 2, self.deletebutton(str(items[i][0]), str(items[i][1])))
                self.tableWidget.setCellWidget(row, 3, self.selectbutton(str(items[i][0]), str(items[i][1])))


    def use(self):
        self.pushButton.clicked.connect(self.ps_Button)


class modify_matter(QMainWindow,Ui_MainWindow_modify_matter):

    def __init__(self):
        super(modify_matter, self).__init__()
        self.setupUi(self)
        self.show()
        self.content_modify()
        self.beforuse()
        self.use()

    def content_modify(self):
        global title, Time, username
        sql = mysql()
        s = "select title,content,remindTime from remind where username='" + username + "'and title='" + title + "' and setTime='"+ Time + "'"
        data = sql.connect(s)[0]
        tim = data[0][2]
        self.lineEdit_title.setText(data[0][0])
        self.plainTextEdit_content.setPlainText(data[0][1])

    def ps_Button_add(self):
        global username,Time
        now = QDate.currentDate()  # 获取当前日期
        datetime = QDateTime.currentDateTime()  # 获取当前日期与时间
        time = QTime.currentTime()  # 获取当前时间
        SetTime = (now.toString(Qt.ISODate) + ' ' + time.toString(Qt.DefaultLocaleLongDate))
        nowtime=time.toString(Qt.DefaultLocaleLongDate).split(':')
        RemindTime = self.timeEdit.text()
        rem=RemindTime.split(':')
        if len(rem[0]) == 1:
            RemindTime1 = '0' + RemindTime
        else:
            RemindTime1 = RemindTime
        Title = self.lineEdit_title.text()
        Content = self.plainTextEdit_content.toPlainText()
        sql = mysql()
        s = "update remind set setTime='"+SetTime+"', title='"+Title+"', remindTime='"+RemindTime1+"', content='"+Content+"' where username='"+username+"' and setTime='"+Time+"'"
        if int(nowtime[0]) == int(rem[0]):
            if int(nowtime[1]) <= int(rem[1]):
                if Title != '' and Content != '':
                    if sql.connect(s)[1] == 1:
                        self.close()
                        msg_box = QMessageBox(QMessageBox.Information, '提示', '修改成功!点击页面刷新')
                        msg_box.exec_()
                    else:
                        msg_box = QMessageBox(QMessageBox.Critical, '错误', '添加失败!')
                        msg_box.exec_()
                else:
                    msg_box = QMessageBox(QMessageBox.Critical, '错误', '标题或内容为空!')
                    msg_box.exec_()
            else:
                msg_box = QMessageBox(QMessageBox.Critical, '错误', '设置时间已经过去请重新设置一个!')
                msg_box.exec_()
        elif int(nowtime[0]) < int(rem[0]):
            if Title != '' and Content != '':
                if sql.connect(s)[1] == 1:
                    self.close()
                    msg_box = QMessageBox(QMessageBox.Information, '提示', '修改成功!点击页面刷新')
                    msg_box.exec_()
                else:
                    msg_box = QMessageBox(QMessageBox.Critical, '错误', '添加失败!')
                    msg_box.exec_()
            else:
                msg_box = QMessageBox(QMessageBox.Critical, '错误', '标题或内容为空!')
                msg_box.exec_()
        else:
            msg_box = QMessageBox(QMessageBox.Critical, '错误', '设置时间已经过去请重新设置一个!')
            msg_box.exec_()

    def use(self):
        self.pushButton_add.clicked.connect(self.ps_Button_add)


class see_matter(QMainWindow,Ui_MainWindow_see_matter):

    def __init__(self):
        super(see_matter, self).__init__()
        self.setupUi(self)
        self.show()
        self.see_text()

    def see_text(self):
        global title,Time,username
        sql = mysql()
        s = "select title,content from remind where username='"+username+"'and title='"+title+"' and setTime='"+Time+"'"
        data = sql.connect(s)[0]
        self.label_title.setText(data[0][0])
        self.plainTextEdit_text.setPlainText(data[0][1])


class remindTips(object):

    def __init__(self):
        super(remindTips, self).__init__()
        self.x()

    def x(self):
        global username,re_time
        re_time=' '
        a = 'False'
        sql = mysql()
        s = "select remindTime from remind where username='"+username+"'and condi='"+a+"' order by remindTime"
        data = sql.connect(s)[0]
        #print(datetime.strftime(data[0],'%H:%M'))
        time = QTime.currentTime()
        now_time = time.toString(Qt.DefaultLocaleLongDate)
        if sql.connect(s)[1]!=0:
            re_time=data[0][0]
            list1 = now_time.split(":")
            now_time_m = (int(list1[0])*60*60+int(list1[1])*60+int(list1[2]))*1000
            list2 = (''.join(data[0])).split(':')
            time1 = (int(list2[0])*60*60+int(list2[1])*60)*1000
            print(data[0][0])
            if now_time_m>time1:
                sql=mysql()
                a="True"
                s="update remind set condi ='"+a+"' where username='"+username+"' and remindTime='"+re_time+"'"
                b=sql.connect(s)[1]
                if b==1:
                    msg_box = QMessageBox(QMessageBox.Information, '提示', '你有一条记录已经超时!点击页面刷新进入下一条')
                    msg_box.exec_()
            else:
                settime=time1-now_time_m
                self.timer_Jump = QTimer()
                self.timer_Jump.timeout.connect(lambda: self.Jumpwindow())  # 计时器挂接到槽：update
                self.timer_Jump.start(settime)
    def Jumpwindow(self):
        global re_time,username
        a = "True"
        s = "update remind set condi='"+a+"' where username='"+username+"' and remindTime='"+re_time+"'"
        sql=mysql()
        sql.connect(s)
        msg_box = QMessageBox(QMessageBox.Information, '提示', '你有一件事情需要处理，请尽快处理!')
        msg_box.exec_()
        self.timer_Jump.stop()



class web(QWidget,Ui_Form):
    tis = ''
    data = ()
    def __init__(self,d,s):
        self.data=d
        self.tis=s
        super(web, self).__init__()
        self.setupUi(self)
        self.mainLayout()
        self.graphical()
        self.initUI()
        self.show()

    def initUI(self):
        self.setWindowTitle(self.tis)
    def graphical(self):
        ls_type=[]
        ls_money=[]
        for i in range(len(self.data)):
            ls_type.append(self.data[i][0])
        for i in range(len(self.data)):
            ls_money.append(self.data[i][1])

        c = (
            Pie()
                .add(
                "",
                [list(z) for z in zip(ls_type, ls_money)],
                radius=["40%", "75%"],
            )
                .set_global_opts(
                title_opts=opts.TitleOpts(title="各类型消费统计"),
                legend_opts=opts.LegendOpts(orient="vertical", pos_top="15%", pos_left="2%"),
            )
                .set_series_opts(label_opts=opts.LabelOpts(formatter="{b}: {c}"))
                .render("pie_radius.html")
        )
    def mainLayout(self):
        self.mainhboxLayout = QHBoxLayout(self)
        self.frame = QFrame(self)
        self.mainhboxLayout.addWidget(self.frame)
        self.hboxLayout = QHBoxLayout(self.frame)

        # 网页嵌入PyQt5
        self.myHtml = QWebEngineView()  ##浏览器引擎控件
        # url = "http://www.baidu.com"
        # 打开本地html文件#使用绝对地址定位，在地址前面加上 file:/// ，将地址的 \ 改为/
        self.myHtml.load(QUrl("file:///pie_radius.html"))

        self.hboxLayout.addWidget(self.myHtml)
        self.setLayout(self.mainhboxLayout)


